package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Сколько вы зарабатываете(в час,указать в $) ");
        int money = in.nextInt();
        if (money >= 8) {
            System.out.println("Сколько вы работаете(в часах в неделю) ");
            int hours = in.nextInt();
            if (hours <= 60) {
                double general = 0;
                double cf = 1;
                int i = 1;
                while (i <= hours) {
                    general += (cf * money);
                    if (i == 40)
                        cf = 1.5;
                    i++;
                }
                System.out.println("Поздравляем,ваш заработок составил: " + general + "$");
            } else {
                System.out.println("Максимум рабочих часов: 60");
            }

        } else {
            System.out.println("Минимальный заработок должен cоставлять: 8$");
        }

    }

}
